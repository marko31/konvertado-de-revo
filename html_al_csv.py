#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Uzu
# ./html_al_csv.py 'html/*' > revo.csv

import sys
import re
import glob

# "dosieroj" estas listo kreita el la unua argumento.

dosieroj = glob.glob(sys.argv[1])
dosieroj.sort()

for dosiero_html in dosieroj :
    
    # Plena legado de eniga dosiero.
    enigo_dosiero = open(dosiero_html, "r")
    enigo = unicode(enigo_dosiero.read(), 'utf-8')
    enigo_dosiero.close() 
    
    # "korpo" estas inter <body> kaj </body>.
    korpo = enigo[(enigo.find("<body>")+6):enigo.rfind("</body>")]
    
    # "vorto" estas antaŭ la unua ";". Neutilaj partoj de vorto estas forigitaj.
    vorto = korpo[:korpo.find(";")]
    vorto = vorto[:vorto.find("<br>")]
    vorto = vorto[:(vorto+",").find(",")]
    vorto = re.sub("<sup>[^<]*</sup>", '', vorto)
    vorto = re.sub("\([^)]*\)", '', vorto)
    vorto = re.sub("<[^>]*>", '', vorto)
    vorto = re.sub(u'[^-a-zA-ZĉĝĥĵŝŭĈĜĤĴŜŬ ]', u'', vorto)
    vorto = vorto.strip()

    literoj = {
        u'ĉ': u'cx',
        u'ĝ': u'gx',
        u'ĥ': u'hx',
        u'ĵ': u'jx',
        u'ŝ': u'sx',
        u'ŭ': u'ux',
        u'Ĉ': u'CX',
        u'Ĝ': u'GX',
        u'Ĥ': u'HX',
        u'Ĵ': u'JX',
        u'Ŝ': u'SX',
        u'Ŭ': u'UX',
    }

    # "vortoX" estas vorto en x-kodo.
    vortoX = ''.join((literoj[l] if l in literoj else l) for l in vorto)

    # "dif" estas difinoj de ĉiuj vortoj en dosiero. 
    dif = korpo[(korpo.find(";")+1):]
    dif = re.sub('[ \t\r\n]+', ' ', dif)
    dif = dif.strip()

    # Tiu programo ĉiam skribas unu eligon por norma esperanta alfabeto.
    # En Kobo oni ne povas tajpi ĉapelitajn literojn, do la sama eligo estas skribita
    # kun la vorto en x-kodo, se la vorto havas ĉapelitajn literojn.
    if (vorto != vortoX) :
        sys.stdout.write ((vortoX + u'\t' + dif + u'\n').encode('utf-8'))

    sys.stdout.write ((vorto + u'\t' + dif + u'\n').encode('utf-8'))

