# !/bin/bash

# Uzu:
# ./rulu.sh "xml/*" xsl_kobo/revohtml.xsl

set -eu

echo "Mi konvertos xml-ajn dosierojn al html-aj dosieroj."

for dosiero_xml in $1; do
    xsltproc -o html/$(basename "$dosiero_xml" .xml).html --path "dtd" $2 $dosiero_xml
done

echo "Mi konvertos html-ajn dosierojn al csv-a dosiero."

mkdir -p vortaro
./html_al_csv.py "html/*.html" > vortaro/revo.csv

echo "Mi kreos vortaron por Kobo."

cd vortaro
python ../penelope/penelope.py --csv -p revo -f eo -t eo --output-kobo

echo 'La dosiero de la vortaro estas en la dosierujo "vortaro".'
